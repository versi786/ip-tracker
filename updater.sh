#!/bin/bash
NOWIPADDR="/home/pi/ip-tracker/nowipaddr"
GETIPADDR=$(dig +short myip.opendns.com @resolver1.opendns.com)
LOG="/home/pi/ip-tracker/ip.log"
timestamp=$( date +%T )
curDate=$( date +"%m-%d-%y" )

if [ -f $NOWIPADDR ]; then
  if [[ $GETIPADDR = $(< $NOWIPADDR) ]]; then
    echo $curDate $timestamp " IP address check: " $(< $NOWIPADDR) >> $LOG
    echo ip stayed the same as $GETIPADDR
  else
    echo $curDate $timestamp " IP address check: " $(< $NOWIPADDR) >> $LOG
    echo $GETIPADDR > $NOWIPADDR
    echo ip changed to $GETIPADDR
    ./update-git.sh
fi
else
  touch $NOWIPADDR
  touch $LOG
  echo $GETIPADDR > $NOWIPADDR
  echo $curDate $timestamp " IP address check: " $(< $NOWIPADDR) >> $LOG
fi
